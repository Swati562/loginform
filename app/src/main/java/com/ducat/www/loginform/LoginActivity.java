package com.ducat.www.loginform;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    FragmentManager mFragmentManager = getSupportFragmentManager();
    FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
    LoginFragment loginFragment = new LoginFragment();
//    SignUpFragment signUpFragment = new SignUpFragment();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        mFragmentTransaction.add(R.id.login_activitylayout,loginFragment);
        mFragmentTransaction.commit();
    }
}
